<?php

namespace sfepy\MasscomBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PageControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
    
    public function testNew()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/admin/page/new');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
    
    public function testNewAuthenticated()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('_submit')->form(array(
          '_username'  => 'admin',
          '_password'  => 'adminpass',
        ));
        $client->submit($form);
        $crawler = $client->followRedirect();
        
        $crawler = $client->request('GET', '/admin/page/new');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
