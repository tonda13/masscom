<?php

namespace sfepy\MasscomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('sfepyMasscomBundle:Page')->findAll();

        return $this->render('sfepyMasscomBundle:Menu:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function docAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('sfepyMasscomBundle:Doc')->findAll();

        return $this->render('sfepyMasscomBundle:Menu:doc.html.twig', array(
            'entities' => $entities,
        ));
    }
    
    public function adminAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('sfepyMasscomBundle:Script')->findAll();

        return $this->render('sfepyMasscomBundle:Menu:admin.html.twig', array(
            'entities' => $entities,
        ));
    }
    
    public function loginAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getUser();
        
        if($user){
            $loggedUser = $em->getRepository('sfepyMasscomBundle:User')->findOneBy(array('username' => $user->getUsername()));
        }else {
            $loggedUser = null;
        }

        return $this->render('sfepyMasscomBundle:Menu:login.html.twig', array(
            'user' => $loggedUser,
        ));
    }
 
}
