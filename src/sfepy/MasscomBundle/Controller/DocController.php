<?php

namespace sfepy\MasscomBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use sfepy\MasscomBundle\Entity\Doc;
use sfepy\MasscomBundle\Form\DocType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Doc controller.
 *
 */
class DocController extends Controller
{

    /**
     * Lists all Doc entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('sfepyMasscomBundle:Doc');
        
        $query = $repository->createQueryBuilder('d')
            ->orderBy('d.menu_order', 'ASC')
            ->getQuery();

        $entities = $query->getResult();

        /* pro každou entitu si vygeneruji delete formulář */
        foreach($entities as $entity){
            $entity->delete = $this->createDeleteForm($entity->getId())->createView();
            $entity->edit = $this->createEditLinkForm($entity->getId())->createView();
        }
        
        return $this->render('sfepyMasscomBundle:Doc:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Doc entity.
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request)
    {
        $entity = new Doc();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        
        //$entity->setCreateAt();
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('sfepy_doc_show', array('slug' => $entity->getSlug())));
        }

        return $this->render('sfepyMasscomBundle:Doc:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Doc entity.
    *
    * @param Doc $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Doc $entity)
    {
        $form = $this->createForm(new DocType(), $entity, array(
            'action' => $this->generateUrl('sfepy_doc_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Create',
            'attr'  => array(
                'class' => 'btn-save'
            )
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Doc entity.
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction()
    {
        $entity = new Doc();
        $form   = $this->createCreateForm($entity);

        return $this->render('sfepyMasscomBundle:Doc:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Doc entity.
     *
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Doc')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doc entity.');
        }

        $deleteForm = $this->createDeleteForm($entity->getId());
        $editLinkForm = $this->createEditLinkForm($entity->getId());

        return $this->render('sfepyMasscomBundle:Doc:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'edit_form_link' => $editLinkForm->createView(), ));
    }

    /**
     * Displays a form to edit an existing Doc entity.
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Doc')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doc entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('sfepyMasscomBundle:Doc:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Doc entity.
    *
    * @param Doc $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Doc $entity)
    {
        $form = $this->createForm(new DocType(), $entity, array(
            'action' => $this->generateUrl('sfepy_doc_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Save & update',
            'attr'  => array(
                'class' => 'btn-save'
            )
        ));

        return $form;
    }
    
    private function createEditLinkForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sfepy_doc_edit', array('id' => $id)))
            ->setMethod('GET')
            ->add('submit', 'submit', array(
                'label' => 'Edit',
                'attr'  => array(
                    'class' => 'btn-edit'
                )
            ))
            ->getForm()
        ;
    }
    
    
    /**
     * Edits an existing Doc entity.
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Doc')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doc entity.');
        }
        
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        
        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('sfepy_doc_edit', array('id' => $id)));
        }

        return $this->render('sfepyMasscomBundle:Doc:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Doc entity.
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('sfepyMasscomBundle:Doc')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Doc entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sfepy_doc'));
    }

    /**
     * Creates a form to delete a Doc entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sfepy_doc_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Delete',
                'attr'  => array(
                    'class' => 'btn-delete'
                )
            ))
            ->getForm()
        ;
    }
}
