<?php

namespace sfepy\MasscomBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use sfepy\MasscomBundle\Entity\Script;
use sfepy\MasscomBundle\Form\ScriptType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Script controller.
 *
 */
class ScriptController extends Controller
{

    /**
     * Lists all Script entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('sfepyMasscomBundle:Script')->findAll();

        return $this->render('sfepyMasscomBundle:Script:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Script entity.
     * 
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request)
    {
        $entity = new Script();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('masscom_script_show', array('id' => $entity->getId())));
        }

        return $this->render('sfepyMasscomBundle:Script:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Script entity.
    *
    * @param Script $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Script $entity)
    {
        $form = $this->createForm(new ScriptType(), $entity, array(
            'action' => $this->generateUrl('masscom_script_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Add new script',
            'attr'  => array(
                'class' => 'btn-save'
            )
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Script entity.
     * 
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction()
    {
        $entity = new Script();
        $form   = $this->createCreateForm($entity);

        return $this->render('sfepyMasscomBundle:Script:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Script entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Script')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Script entity.');
        }
        
        $problems = $em->getRepository('sfepyMasscomBundle:Problem')->findByScript($id);

        $deleteForm = $this->createDeleteForm($id);
        $editLinkForm = $this->createEditLinkForm($id);

        return $this->render('sfepyMasscomBundle:Script:show.html.twig', array(
            'entity'            => $entity,
            'delete_form'       => $deleteForm->createView(),
            'edit_form_link'    => $editLinkForm->createView(),
            'problems'          => $problems, ));
    }

    /**
     * Displays a form to edit an existing Script entity.
     * 
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Script')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Script entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('sfepyMasscomBundle:Script:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Script entity.
     * 
    * @Security("has_role('ROLE_ADMIN')")
    * 
    * @param Script $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Script $entity)
    {
        $form = $this->createForm(new ScriptType(), $entity, array(
            'action' => $this->generateUrl('masscom_script_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Save & update',
            'attr'  => array(
                'class' => 'btn-save'
            )
        ));

        return $form;
    }
    
    private function createEditLinkForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('masscom_script_edit', array('id' => $id)))
            ->setMethod('GET')
            ->add('submit', 'submit', array('label' => 'Edit'))
            ->getForm()
        ;
    }
    
    /**
     * Edits an existing Script entity.
     * 
     * @Security("has_role('ROLE_ADMIN')")
     * 
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Script')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Script entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('masscom_script_edit', array('id' => $id)));
        }

        return $this->render('sfepyMasscomBundle:Script:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    /**
     * Deletes a Script entity.
     * 
     *  @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('sfepyMasscomBundle:Script')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Script entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('masscom_script'));
    }

    /**
     * Creates a form to delete a Script entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('masscom_script_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
            'label' => 'Delete',
            'attr'  => array(
                'class' => 'btn-delete'
            )
        ))
            ->getForm()
        ;
    }
}
