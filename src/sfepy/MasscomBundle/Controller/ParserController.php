<?php

namespace sfepy\MasscomBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use sfepy\MasscomBundle\Form\ParserType;

use \Exception;

class ParserController extends Controller
{
    public function formAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $problem = $em->getRepository('sfepyMasscomBundle:Problem')->findOneById($id);
        
        if (!$problem) {
            throw $this->createNotFoundException('Unable to find Problem entity.');
        }
        
        require_once 'lib/ganon.php';
        $xml_dom = str_get_dom($problem->getXmlSchema());
        
        foreach($xml_dom('problem') as $prb) {
            foreach($prb->attributes as $attribute => $value) {
                $xml_problem[$attribute] = $value;
            }
        }
        
        /* Získá všechny elementy <vars> sloužící jako předefinované proměnné pro výpočet */
        foreach($xml_dom('variables > var') as $n => $element) {
            //získá celý element (full XML tag)
            $xml_variable[$n]['full'] = $element->html();
            //získá obsah elementu
            $xml_variable[$n]['attribute']['value'] = $element->getInnerText();
            
            //získá všechny atributy elementu
            foreach($element->attributes as $attribute => $value) {
                $xml_variable[$n]['attribute'][$attribute] = $value;
            }
        }
        
        /* Získá všechny elementy <form> sloužící pro generování formuláře */
        $xml_form = array();
        foreach($xml_dom('form > (field, text)') as $n => $element) {
            //získá celý element (full XML tag)
            $xml_form[$n]['full'] = $element->html();
            $xml_form[$n]['tag'] = $element->tag;
            //získá obsah elementu
            $xml_form[$n]['attribute']['value'] = $element->getInnerText();
            
            //získá všechny atributy elementu
            foreach($element->attributes as $attribute => $value) {
                $xml_form[$n]['attribute'][$attribute] = $value;
            }
        }
        
        foreach($xml_form as $n => $form){
            if($form['attribute']['type'] == "combo"){
                $subdom = str_get_dom($form['attribute']['value']);
                $xml_form[$n]['attribute']['test'] = 'test';
                foreach($subdom('opt') as $k => $element) {
                    $xml_form[$n]['attribute']['opt'][$k]['name'] = $element->getInnerText();
                    
                    foreach($element->attributes as $attribute => $value) {
                        $xml_form[$n]['attribute']['opt'][$k][$attribute] = $value;
                    }
                }
            }
        }
        
        
        
        $inputForm = $this->createInputForm($id, $xml_problem['script'] , $xml_variable, $xml_form);
        
        return $this->render('sfepyMasscomBundle:Parser:index.html.twig', array(
            'xml_problem'   => $xml_problem,
            'xml_vars'      => $xml_variable,
            'xml_forms'     => $xml_form,
            'input_form'    => $inputForm->createView(),
        ));
    }
    
    /**
    * Creates a form to choosing script options.
    *
    * @param $problem
    * @param $xml_vars
    * @param $xml_form_data
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createInputForm($id, $problem, $xml_vars, $xml_form_data)
    {
        $form = $this->createForm(new ParserType($id, $problem, $xml_vars, $xml_form_data), null, array(
            'action' => $this->generateUrl('sfepy_parser_getpython'),
            'method' => 'POST',
            'attr' => array (
                'class' => 'parse_form',
            ),
        ));
      
        $form->add('compute', 'submit', array('label' => 'Compute'));
        $user = $this->getUser();
        if(isset($user)){
            if($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('ROLE_SUPER_ADMIN')){
                $form->add('getPython', 'submit', array('label' => 'Generate Py script'));
            }
        }
        $form->add('save', 'submit', array('label' => 'Save to my repository'));

        return $form;
    }
    
    /**
     * Generate a new Python script from Form data
     */
    public function getpythonAction(Request $request)
    {
        if ('POST' === $request->getMethod())
        {
            $form = $request->request->all();
            $data = $form['sfepy_masscombundle_parser'];
            $vars = array(); //skrité proměnné pro generování scriptu
            $prop = array(); //uživatelem zvolené hodnoty
            $other = array(); //ostatní pole
            $filename_mesh = array(); //data vazajácí se ke zvolenému filename_mesh   
            
            //procházení všech došlých hodnot
            foreach($data as $key => $value){
                $expl = explode("__", $key); //rozparsování jména formulář. prvku podle "__"
                
                //ošetření datových typů zadaných proměnných
                if(count($expl) > 2  && $expl[1] == "data"){
                    switch ($expl[2]) {
                        case "float":
                            if(0 == floatval($value)){
                                $this->get('session')->getFlashBag()->add(
                                    'error',
                                    'Variable '.$expl[0].' isn\'t float!'
                                );
                                return $this->forward('sfepyMasscomBundle:Parser:form', array(
                                    'id'  => $data['id'],
                                ));
                            }break;
                        case "vect":
                            if(0 == floatval($value)){
                                $this->get('session')->getFlashBag()->add(
                                    'error',
                                    'Vector variable '.$expl[0].$expl[3].' isn\'t number!'
                                );
                                return $this->forward('sfepyMasscomBundle:Parser:form', array(
                                    'id'  => $data['id'],
                                ));
                            }break;
                        case "int":
                            if(0 == intval($value)){
                                $this->get('session')->getFlashBag()->add(
                                    'error',
                                    'Variable '.$expl[0].' isn\'t integer!'
                                );
                                return $this->forward('sfepyMasscomBundle:Parser:form', array(
                                    'id'  => $data['id'],
                                ));
                            }break;
                        case "str":
                            if(intval($value) || floatval($value)){
                                $this->get('session')->getFlashBag()->add(
                                    'error',
                                    'Variable '.$expl[0].' isn\'t string!'
                                );
                                return $this->forward('sfepyMasscomBundle:Parser:form', array(
                                    'id'  => $data['id'],
                                ));
                            }break;
                    }
                }
                
                //jedná se o uživatelem vyplněné hodnoty
                if(count($expl) > 1 && $expl[1] == "data"){
                    //jedná se o vektor (je potřeba přidat ještě souřadnici X, Y nebo Z)
                    if(count($expl) > 3 && $expl[2] == "vect"){
                        $var_name = $expl[0].strtoupper($expl[3]);
                    }else {
                        $var_name = $expl[0];
                    }
                    $prop[$var_name] = $value; 
                    
                    //příprava pole pro náhradu uživatelských dat ve skriptu
                    $a[] = "%".$var_name;
                    $b[] = $value;
                }
                
                //jedná se skrytá pole
                else if(count($expl) > 1 && $expl[1] == "var"){
                    if($expl[0] == "import"){
                        $import = $value;
                    }elseif(array_key_exists($expl[0], $vars)){
                        $vars[$expl[0]] .= "\t" .$value. ", \n ";
                    }else {
                        $vars[$expl[0]] = "" .$expl[0]. " = {\n\t" .$value. ", \n ";
                    }
                }
                
                //jedná se o data náležící k vybranému filename_mesh
                else if(count($expl) > 2 && $expl[1] == "filename_mesh"){
                    $filename_mesh[$expl[2]][$expl[0]] = $value;
                }
                
                //všechna zbylá pole
                else {
                    $other[$key] = $value;
                }
            }
            
            //závěrečná úprava skrytých dat
            foreach($vars as $k => $var){
                $vars[$k] .= "}\n\n";
            }
            /* 
             * - závěrečná úprava filename_mesh
             * - vyberu jen data, která patří k aktuálně zvolenému filename_mesh
             */
            foreach($filename_mesh as $value){
                if($value['value']  == $prop["filename_mesh"]){
                    foreach($value as $key => $val){
                        if($key != "value"){ 
                            $mesh_data[$key] = $val;
                            //příprava pole pro náhradu uživatelských dat ve skriptu
                            $a[] = "%".$key;
                            $b[] = $val;
                        }
                    }
                }
            }
            
            //sloučí všechny $vars do jednoho stringu
            
            $s = (isset($import) ? $import."\n\n" : "")."filename_mesh = '%filename_mesh'\n\n";
            foreach($vars as $var){
                $s .= $var;
            }
            
            //nahrazení %promenna za patřičné hodnoty
            $script = str_replace($a, $b, $s);
        }
        if(isset($data['getPython'])){
            return $this->render('sfepyMasscomBundle:Parser:getpython.html.twig', array(
                'script'=> $script,
                'data'  => $prop,
                'other' => $other,
                'filename_mesh' => $mesh_data,
            ));
        }elseif(isset($data['save'])){
            //TODO: dodělat ukládání problému
            echo("SAVING");exit;
        }else {
            //Compute
            $hash = md5(time().$script);
                //$hash = "064b74946b1ee85d9057bcf68d7c65d3";
            $dir = "../sfepy_input/";
            $file = $dir."input_".$hash.".py";
            
            /* vytvoření souboru, pokud se to nezdaří dojde k přesměrování zpět na formulář */
            if(file_put_contents($file, $script) === FALSE){
                $this->get('session')->getFlashBag()->add(
                    'error',
                    'Server cannot create file!'
                );
                return $this->forward('sfepyMasscomBundle:Parser:form', array(
                    'id'  => $data['id'],
                ));
            }
            
            set_include_path('lib/');
            require_once('lib/Net/SSH2.php');
            
            $em = $this->getDoctrine()->getManager();
            $problem = $em->getRepository('sfepyMasscomBundle:Problem')->findOneById($data['id']);
            if (!$problem) {
                throw $this->createNotFoundException('Unable to find Problem entity.');
            }
            
            $server = $problem->getScript()->getUrl();
            $username = "sfepyweb";
            $password = "sf3pyw3b";
            $cmd = str_replace("HASH", $hash, $problem->getScript()->getCmd()); //co, cim, kde
            /*
            $connection = ssh2_connect('$server', 22);
            ssh2_auth_password($connection, '$username', '$password');
            $stream = ssh2_exec($connection, 'ls');
            var_dump($stream);exit;
            */

            $ssh = new \Net_SSH2($server);
            if (!$ssh->login($username, $password)) {
                $this->get('session')->getFlashBag()->add(
                    'error',
                    'Cannot connect server '.$server.' via SSH!'
                );
                return $this->forward('sfepyMasscomBundle:Parser:form', array(
                    'id'  => $data['id'],
                ));
            }
            
            //rozpoznání lokálního serveru
            if (isset($_SERVER['HTTP_CLIENT_IP'])
                || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
                || !in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1'))
            ) {
                $ssh->exec($cmd);
            }else {
                $ssh->exec("./run_sfepy abc");
                $dir = "http://sfepy.org/masscom/sfepy_input/";
                $hash = "abc";
            }
            restore_include_path();
            
            
            $img = $dir."output_".$hash.".png";
            if(!file_exists($img)){
                $imgexist = false;
            }
            
            return $this->render('sfepyMasscomBundle:Parser:visualresult.html.twig', array(
                'img'  => $img,
                'imgexist'  => $imgexist,
            ));
        }
    }
}
