<?php

namespace sfepy\MasscomBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use sfepy\MasscomBundle\Entity\Usersave;
use sfepy\MasscomBundle\Form\UsersaveType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\HttpKernel\Exception\HttpException;


/**
 * Usersave controller.
 *
 */
class UsersaveController extends Controller
{

    /**
     * Lists all Usersave entities.
     * 
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $entities = $em->getRepository('sfepyMasscomBundle:Usersave')->findByUser($user->getId());

        return $this->render('sfepyMasscomBundle:Usersave:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    
    /**
     * Creates a new Usersave entity.
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request)
    {
        $entity = new Usersave();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('sfepy_save_show', array('id' => $entity->getId())));
        }

        return $this->render('sfepyMasscomBundle:Usersave:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
    
    /**
     * Creates a new Usersave entity and save into database.
     */
    public function saveAction(Problem $problem, $datafile)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = new Usersave();
        $entity->setName(new \Date('d-m-Y H:i'));
        $entity->setProblem($problem);
        $entity->setDatafile($datafile);
        
        $user = $this->getUser();
        $entity->setUser($user);

        $em->persist($entity);
        $em->flush();

        return $this;
    }

    /**
    * Creates a form to create a Usersave entity.
    *
    * @param Usersave $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Usersave $entity)
    {
        $form = $this->createForm(new UsersaveType(), $entity, array(
            'action' => $this->generateUrl('sfepy_save_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Create',
            'attr'  => array(
                'class' => 'btn-save'
            )
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Usersave entity.
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
     public function newAction()
    {
        $entity = new Usersave();
        $form   = $this->createCreateForm($entity);

        return $this->render('sfepyMasscomBundle:Usersave:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Usersave entity.
     *
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Usersave')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usersave entity.');
        }
        
        //get current logged user
        $user = $this->getUser();
        
        if($user != $entity->getUser()){
            throw new HttpException(403, "You don't have permission for display this Usersave entity!");
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('sfepyMasscomBundle:Usersave:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Usersave entity.
     *
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Usersave')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usersave entity.');
        }
        
        //get current logged user
        $user = $this->getUser();
        
        if($user != $entity->getUser()){
            throw new HttpException(403, "You don't have permission for editing this Usersave entity!");
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('sfepyMasscomBundle:Usersave:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Usersave entity.
    *
    * @param Usersave $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Usersave $entity)
    {
        $form = $this->createForm(new UsersaveType(), $entity, array(
            'action' => $this->generateUrl('sfepy_save_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Save & update',
            'attr'  => array(
                'class' => 'btn-save'
            )
        ));

        return $form;
    }
    /**
     * Edits an existing Usersave entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Usersave')->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usersave entity.');
        }
        
        //get current logged user
        $user = $this->getUser();
        
        if($user != $entity->getUser()){
            throw new HttpException(403, "You don't have permission for upadating this Usersave entity!");
        }
       
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('sfepy_save_edit', array('id' => $id)));
        }

        return $this->render('sfepyMasscomBundle:Usersave:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Usersave entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('sfepyMasscomBundle:Usersave')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Usersave entity.');
            }
            
            //get current logged user
            $user = $this->getUser();

            if($user != $entity->getUser()){
                throw new HttpException(403, "You don't have permission for delete this Usersave entity!");
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sfepy_save'));
    }

    /**
     * Creates a form to delete a Usersave entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sfepy_save_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Delete',
                'attr'  => array(
                    'class' => 'btn-delete'
                )
            ))
            ->getForm()
        ;
    }
}
