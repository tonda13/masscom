<?php

namespace sfepy\MasscomBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use sfepy\MasscomBundle\Entity\Problem;
use sfepy\MasscomBundle\Form\ProblemType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Problem controller.
 *
 */
class ProblemController extends Controller
{

    /**
     * Lists all Problem entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $repository = $this->getDoctrine()->getRepository('sfepyMasscomBundle:Problem');

        /*$query = $repository->createQueryBuilder('p')
            ->groupBy('p.script')
            ->getQuery();

        $entities = $query->getResult();*/

        $query = $repository->createQueryBuilder('p')
            ->orderBy('p.script', 'DESC')
            ->getQuery();

        $entities = $query->getResult();
        
        //$entities = $repository->findAll();

        return $this->render('sfepyMasscomBundle:Problem:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Problem entity.
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request)
    {
        $entity = new Problem();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('sfepy_problem_show', array('id' => $entity->getId())));
        }

        return $this->render('sfepyMasscomBundle:Problem:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Problem entity.
    * 
    * @param Problem $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Problem $entity)
    {
        $form = $this->createForm(new ProblemType(), $entity, array(
            'action' => $this->generateUrl('sfepy_problem_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
                'label' => 'Add a new problem',
                'attr'  => array(
                    'class' => 'btn-save'
                )
            ));

        return $form;
    }

    /**
     * Displays a form to create a new Problem entity.
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction()
    {
        $entity = new Problem();
        $form   = $this->createCreateForm($entity);

        return $this->render('sfepyMasscomBundle:Problem:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Problem entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Problem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Problem entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editLinkForm = $this->createEditLinkForm($id);

        return $this->render('sfepyMasscomBundle:Problem:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'edit_form_link' => $editLinkForm->createView(), ));
    }

    /**
     * Displays a form to edit an existing Problem entity.
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Problem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Problem entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('sfepyMasscomBundle:Problem:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Problem entity.
    *
    * @param Problem $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Problem $entity)
    {
        $form = $this->createForm(new ProblemType(), $entity, array(
            'action' => $this->generateUrl('sfepy_problem_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Save & update',
            'attr'  => array(
                'class' => 'btn-save'
            )
        ));

        return $form;
    }
    
    private function createEditLinkForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sfepy_problem_edit', array('id' => $id)))
            ->setMethod('GET')
            ->add('submit', 'submit', array(
                'label' => 'Edit',
                'attr'  => array(
                    'class' => 'btn-edit'
                )
            ))
            ->getForm()
        ;
    }
    
    /**
     * Edits an existing Problem entity.
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('sfepyMasscomBundle:Problem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Problem entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            //this save a log
            $this->forward('sfepyMasscomBundle:Log:save', array(
                'problem'  => $entity,
                'success' => 'true',
            ));
            
            return $this->redirect($this->generateUrl('sfepy_problem_show', array('id' => $id)));
        }

        return $this->render('sfepyMasscomBundle:Problem:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Problem entity.
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('sfepyMasscomBundle:Problem')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Problem entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sfepy_problem'));
    }

    /**
     * Creates a form to delete a Problem entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sfepy_problem_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Delete',
                'attr'  => array(
                    'class' => 'btn-delete'
                )
            ))
            ->getForm()
        ;
    }
}
