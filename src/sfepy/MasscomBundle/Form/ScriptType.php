<?php

namespace sfepy\MasscomBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ScriptType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('shortDesc', 'textarea', array(
                'label' => 'Short description',
                'attr' => array(
                    'rows' => '10',
                    'style' => 'width: 600px;',
                    'class' => 'tinymce',
                ),
                ))
            ->add('url', 'text', array(
                'label' => 'URL',
                'required' => false,
                ))
            ->add('cmd', 'text', array(
                'label' => 'Command',
                'required' => false,
                ))
            ->add('fulldesc', 'textarea', array(
                'label' => 'Full of description',
                'required' => false,
                'attr' => array(
                    'rows' => '20',
                    'style' => 'width: 600px;',
                    'class' => 'tinymce',
                ),
                ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sfepy\MasscomBundle\Entity\Script',
            'attr' => array('novalidate' => 'novalidate')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sfepy_masscombundle_script';
    }
}
