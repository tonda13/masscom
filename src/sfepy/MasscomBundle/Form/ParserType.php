<?php

namespace sfepy\MasscomBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParserType extends AbstractType
{
    private $id;
    private $problem;
    private $vars;
    private $forms;
    
    public function __construct($id, $problem, $xml_vars, $xml_form_data) {
        $this->id = $id;
        $this->problem = $problem;
        $this->vars = $xml_vars;
        $this->forms = $xml_form_data;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('id', 'hidden', array(
            'data' => $this->id,
        ));
        
        //pole problém
        $builder->add('problem', 'hidden', array(
            'data' => $this->problem,
        ));

        //výpis skrytých polí vars

        foreach($this->vars as $w => $var){
            if(!empty($var['attribute']['type'])){
                if(isset($var['attribute']['name']) && !empty($var['attribute']['name'])){
                    $formName = $var['attribute']['name'];
                } else {
                    $formName = $var['attribute']['type']; 
                }

                $builder->add($formName.'__var__'.sprintf("%02d",$w), 'hidden', array(
                    'data' => $var['attribute']['value'],
                    'attr' => array(
                        'data-type' =>  $var['attribute']['type'],
                    ),
                ));
            }
        }
        
        
        foreach($this->forms as $q => $form){
            //výpis obyčejného textu přes label pomocí Genemu
            if(!empty($form['attribute']['type']) && $form['tag'] == "text"){
                if($form['attribute']['type'] == "subtitle"){
                    $opt = array(
                        'style' => 'font-size: 150%;'
                    );
                } else {
                    $opt = array();
                }
                    
                $builder->add('text_'.$q, 'genemu_plain', array(
                    'label' => $form['attribute']['value'],
                    'label_attr' => $opt,
                    'required'  => false,
                ));
                
            //výpis select boxu (combobox)
            }elseif(!empty($form['attribute']['type']) && $form['attribute']['type'] == "combo"){
                //var_dump($form['attribute']);exit();
                foreach($form['attribute']['opt'] as $k => $opt){
                    $choices[$opt['value']] = $opt['name'];
                }
                $builder->add($form['attribute']['name'].'__data', 'choice', array(
                    'choices'   => $choices,
                ));
                
                //strašlivá konstrukce pro zachycení hodnot u filename_mesh
                foreach($form['attribute']['opt'] as $k => $opt){
                    if($form['attribute']['name'] == "filename_mesh"){
                        foreach($opt as $key => $val){
                            if($key != "name"){
                                $builder->add($key.'__'.$form['attribute']['name'].'__'.$k, 'hidden', array(
                                    'required'  => false,
                                    'data'      => $val,
                                ));
                            }
                        }
                    }
                }
                
                
            
            //výpis pole typu vector
            }elseif(!empty($form['attribute']['type']) && $form['attribute']['type'] == "vect"){
                $builder->add('text_'.$q, 'genemu_plain', array(
                    'label' => $form['attribute']['value'],
                    'required'  => false,
                ));
                $builder->add($form['attribute']['name'].'__data__vect__x', 'number', array(
                    'label' => 'X (float): ',
                    'label_attr' => array('style' => 'float: right;'),
                    'attr'  => array('class' => 'vector'),
                ));
                $builder->add($form['attribute']['name'].'__data__vect__y', 'number', array(
                    'label' => 'Y (float): ',
                    'label_attr' => array('style' => 'float: right;'),
                    'attr'  => array('class' => 'vector'),
                ));
                $builder->add($form['attribute']['name'].'__data__vect__z', 'number', array(
                    'label' => 'Z (float): ',
                    'label_attr' => array('style' => 'float: right;'),
                    'attr'  => array('class' => 'vector'),
                ));

            //výpis ostatních polí
            }else/*if(!empty($form['attribute']['type']) && $form['attribute']['type'] != "combo")*/{
                $builder->add($form['attribute']['name'].'__data__'.$form['attribute']['type'],
                    'text',
                        array(
                        'label' => $form['attribute']['value']. " (".$form['attribute']['type'].")",
                    ));
            }
        }
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
//        $resolver->setDefaults(array(
//            'data_class' => 'sfepy\MasscomBundle\Entity\Problem'
//        ));
        $resolver->setDefaults(array(
            'validation_groups' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sfepy_masscombundle_parser';
    }
}
