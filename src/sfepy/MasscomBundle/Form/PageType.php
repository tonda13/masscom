<?php

namespace sfepy\MasscomBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => 'Titulek',
                ))
            ->add('slug', 'text', array(
                'label' => 'URL slug',
            ))
            ->add('text_field', 'textarea', array(
                'label' => 'Obsah',
                'attr' => array(
                    'rows' => '20',
                    'style' => 'width: 600px;',
                    'class' => 'tinymce',
                ),
            ))
            ->add('menu_order', 'integer', array(
                'label' => 'Menu order',
            ))
            //->add('create_at')
            //->add('update_at')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sfepy\MasscomBundle\Entity\Page',
            'attr' => array('novalidate' => 'novalidate')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sfepy_masscombundle_page';
    }
}
