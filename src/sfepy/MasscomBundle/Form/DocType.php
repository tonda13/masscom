<?php

namespace sfepy\MasscomBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('slug')
            ->add('text_field', 'textarea', array(
                'label' => 'Text',
                'attr' => array(
                    'rows' => '35',
                    'style' => 'width: 600px;',
                    'class' => 'tinymce',
                ),
                ))
            ->add('menu_order')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sfepy\MasscomBundle\Entity\Doc',
            'attr' => array('novalidate' => 'novalidate')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sfepy_masscombundle_doc';
    }
}
