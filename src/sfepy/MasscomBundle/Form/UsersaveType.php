<?php

namespace sfepy\MasscomBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsersaveType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('datafile', 'textarea', array(
                'label' => 'XML scheme',
                'attr' => array(
                    'rows' => '35',
                    'style' => 'width: 600px;'
                ),
            ))
            ->add('problem')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sfepy\MasscomBundle\Entity\Usersave'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sfepy_masscombundle_usersave';
    }
}
