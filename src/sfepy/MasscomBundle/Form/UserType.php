<?php

namespace sfepy\MasscomBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('name')
            ->add('lastname')
            ->add('email')
            ->add('enabled', 'checkbox', array(
                'label'     => 'Enabled',
                'required'  => false,
            ))
            ->add('locked', 'checkbox', array(
                'label'     => 'Locked',
                'required'  => false,
            ))
            ->add('expired', 'checkbox', array(
                'label'     => 'Expired',
                'required'  => false,
            ))
            ->add('roles')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sfepy\MasscomBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sfepy_masscombundle_user';
    }
}
