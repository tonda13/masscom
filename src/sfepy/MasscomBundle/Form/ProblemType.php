<?php

namespace sfepy\MasscomBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProblemType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('shortDesc', 'textarea', array(
                'label' => 'Short description',
                'attr' => array(
                    'rows' => '10',
                    'style' => 'width: 600px;',
                    'class' => 'tinymce',
                ),
                'required' => false,
                ))
            ->add('xml_schema', 'textarea', array(
                'label' => 'XML scheme',
                'attr' => array(
                    'rows' => '35',
                    'style' => 'width: 600px;'
                ),
                )) 
            ->add('script', 'entity', array(
                'class' => 'sfepyMasscomBundle:Script',
                'empty_value' => 'Choose a script',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sfepy\MasscomBundle\Entity\Problem',
            'attr' => array('novalidate' => 'novalidate')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sfepy_masscombundle_problem';
    }
}
