<?php
//load data into DB with: php app/console doctrine:fixtures:load
namespace sfepy\MasscomBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use sfepy\MasscomBundle\Entity\Log;

class LoadLogData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $log1 = new Log();
        $log1->setUser($this->getReference('admin'));
        $log1->setProblem($this->getReference('problem1'));
        $log1->setCreateAt(new \DateTime('NOW'));
        $log1->setSuccess(true);
        $this->addReference('log1', $log1);

        $log2 = new Log();
        $log2->setUser($this->getReference('admin'));
        $log2->setProblem($this->getReference('problem2'));
        $log2->setCreateAt(new \DateTime('NOW'));
        $log2->setSuccess(false);
        $this->addReference('log2', $log2);
        
        $log3 = new Log();
        $log3->setUser($this->getReference('user'));
        $log3->setProblem($this->getReference('problem3'));
        $log3->setCreateAt(new \DateTime('NOW'));
        $log3->setSuccess(true);
        $this->addReference('log3', $log3);
        
        $log4 = new Log();
        $log4->setUser($this->getReference('admin'));
        $log4->setProblem($this->getReference('problem2'));
        $log4->setCreateAt(new \DateTime('NOW'));
        $log4->setSuccess(true);
        $this->addReference('log4', $log4);
        
        $em->persist($log1);
        $em->persist($log2);
        $em->persist($log3);
        $em->persist($log4);
        
        $em->flush();

   
    }

    public function getOrder()
    {
        return 7; // the order in which fixtures will be loaded
    }
}
