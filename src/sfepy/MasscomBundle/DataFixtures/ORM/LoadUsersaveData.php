<?php
//load data into DB with: php app/console doctrine:fixtures:load
namespace sfepy\MasscomBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use sfepy\MasscomBundle\Entity\Usersave;

class LoadUsersaveData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $save1 = new Usersave();
        $save1->setName('test 1');
        $save1->setUser($this->getReference('admin'));
        $save1->setProblem($this->getReference('problem1'));
        $save1->setDatafile('XML');
        $this->addReference('save1', $save1);

        $save2 = new Usersave();
        $save2->setName('test 2');
        $save2->setUser($this->getReference('user'));
        $save2->setProblem($this->getReference('problem2'));
        $save2->setDatafile('XML');
        $this->addReference('save2', $save2);
        
        $save3 = new Usersave();
        $save3->setName('test 3');
        $save3->setUser($this->getReference('admin'));
        $save3->setProblem($this->getReference('problem3'));
        $save3->setDatafile('XML');
        $this->addReference('save3', $save3);
        
        $em->persist($save1);
        $em->persist($save2);
        $em->persist($save3);
        
        $em->flush();

   
    }

    public function getOrder()
    {
        return 6; // the order in which fixtures will be loaded
    }
}
