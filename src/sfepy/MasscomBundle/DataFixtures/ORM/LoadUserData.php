<?php
//load data into DB with: php app/console doctrine:fixtures:load
namespace sfepy\MasscomBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use sfepy\MasscomBundle\Entity\User;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }
    
    public function load(ObjectManager $em)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        // Create a new user
        $user1 = $userManager->createUser();
        $user1->setUsername('admin');
        $user1->setName('Super');
        $user1->setLastname('User');
        $user1->setEmail('admin@example.com');
        $user1->setPlainPassword('adminpass');
        $user1->setEnabled(true);
        $user1->setRoles(array('ROLE_SUPER_ADMIN'));
        $this->addReference('admin', $user1);

        $user2 = $userManager->createUser();
        $user2->setUsername('user');
        $user2->setName('John');
        $user2->setLastname('Doe');
        $user2->setEmail('user@example.com');
        $user2->setPlainPassword('userpass');
        $user2->setEnabled(true);
        $this->addReference('user', $user2);
        
        $em->persist($user1);
        $em->persist($user2);
        $em->flush();
    }

    public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }
}
