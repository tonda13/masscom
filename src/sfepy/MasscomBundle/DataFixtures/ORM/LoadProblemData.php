<?php
//load data into DB with: php app/console doctrine:fixtures:load
namespace sfepy\MasscomBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use sfepy\MasscomBundle\Entity\Problem;

class LoadProblemData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $problem1 = new Problem();
        $problem1->setName('Linear elastic');
        $problem1->setXmlSchema('<problem name="Linear Elasticity" script="linear_elastic">
        <variables>
          <!--
              oblasti
          -->
          <var type="str" name="regions">\'Omega\': \'all\'</var>
          <var type="str" name="regions">\'Left\': (\'vertices in (x < %region_left)\', \'facet\')</var>
          <var type="str" name="regions">\'Right\': (\'vertices in (x > %region_right)\', \'facet\')</var>

          <!--
              materiál
          -->
          <var type="str" name="materials">\'solid\': ({\'lam\': %lambda, \'mu\': %mu},)</var>

          <!--
              pole (posuvy, rychlosti, teplota, ...)
          -->
          <var type="str" name="fields">\'displacement\': (\'real\', \'vector\', \'Omega\', 1)</var>

          <!--
              neznámé, testovací a parametrické proměnné
          -->
          <var type="str" name="variables">\'u\': (\'unknown field\', \'displacement\')</var>
          <var type="str" name="variables">\'v\': (\'test field\', \'displacement\', \'u\')</var>

          <!--
          definice integrálů
          -->
          <var type="str" name="integrals">\'i\': 2,</var>

          <!--
          okrajové podmínky
          -->
          <var type="str" name="ebcs">\'Fixed\': (\'Left\', {\'u.all\': 0.0})</var>
          <var type="str" name="ebcs">\'Displaced\': (\'Right\', {\'u.0\': %u_rightX, \'u.1\': %u_rightY, \'u.2\': %u_rightZ})</var>

          <!--
              rovnice problému
          -->
          <var type="str" name="equations">\'balance_of_forces\': """dw_lin_elastic_iso.i.Omega( solid.lam, solid.mu, v, u ) = 0"""</var>
          <!--
              použité řešiče
          -->
          <var type="str" name="solver">\'ls\': (\'ls.scipy_direct\', {})</var>
          <var type="str" name="solver">\'newton\': (\'nls.newton\', {\'eps_a\': 1e-10, \'eps_r\': 1.0, \'check\': 0, \'problem\': \'nonlinear\'})</var>

        </variables>
        <form>

          <text type="subtitle">Geometrie</text>
          <field type="combo" name="filename_mesh" label="Geometrie">
            <opt value="cylinder.mesh" region_left="0.001" region_right="0.099">Válec</opt>
            <opt value="cube_medium_hexa.mesh" region_left="-0.499" region_right="0.499">Krychle</opt>
            <opt value="block.mesh" region_left="-4.99" region_right="4.99">Kvádr</opt>
          </field>

          <!--
              volba materiálových parametrů
          -->
          <text type="subtitle">Materiálové parametry</text>
          <field type="float" name="lambda">Lambda</field>
          <field type="float" name="mu">Mu</field>

          <!--
              okrajové podmínky - posuv na pravé hranici
          -->
          <text type="subtitle">Okrajové podmínky</text>
          <field type="vect" name="u_right">Posuvy na pravé stěně</field>
        </form>
      </problem>');
        $problem1->setScript($this->getReference('scriptSfepy'));
        $this->addReference('problem1', $problem1);

        $problem2 = new Problem();
        $problem2->setName('Test 2');
        $problem2->setXmlSchema('<problem></problem>');
        $problem2->setScript($this->getReference('scriptSfepy'));
        $this->addReference('problem2', $problem2);
        
        $problem3 = new Problem();
        $problem3->setName('Test 3');
        $problem3->setXmlSchema('<problem></problem>');
        $problem3->setScript($this->getReference('scriptSfepy'));
        $this->addReference('problem3', $problem3);
        
        $problem4 = new Problem();
        $problem4->setName('Test 4');
        $problem4->setXmlSchema('<problem></problem>');
        $problem4->setScript($this->getReference('script2'));
        $this->addReference('problem4', $problem4);
        
        $em->persist($problem1);
        $em->persist($problem2);
        $em->persist($problem3);
        $em->persist($problem4);

        $em->flush();

   
    }

    public function getOrder()
    {
        return 5; // the order in which fixtures will be loaded
    }
}
