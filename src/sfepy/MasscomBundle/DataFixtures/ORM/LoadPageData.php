<?php
//load data into DB with: php app/console doctrine:fixtures:load
namespace sfepy\MasscomBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use sfepy\MasscomBundle\Entity\Page;

class LoadPageData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $page1 = new Page();
        $page1->setTitle('Úvod');
        $page1->setSlug('uvod');
        $page1->setTextField('Toto je úvodní stránka...');
        $page1->setMenuOrder(1);
        $page1->setCreateAt(new \DateTime('NOW'));
        $page1->setUpdateAt(new \DateTime('NOW'));
        $this->addReference('page1', $page1);
        
        $page2 = new Page();
        $page2->setTitle('O nás');
        $page2->setSlug('o-nas');
        $page2->setTextField('Toto je stránka s informacemi o autorovi...');
        $page2->setMenuOrder(2);
        $page2->setCreateAt(new \DateTime('NOW'));
        $page2->setUpdateAt(new \DateTime('NOW'));
        $this->addReference('page2', $page2);
        
        $page3 = new Page();
        $page3->setTitle('FAQ');
        $page3->setSlug('faq');
        $page3->setTextField('Stránka s často kladenými dotazy...');
        $page3->setMenuOrder(3);
        $page3->setCreateAt(new \DateTime('NOW'));
        $page3->setUpdateAt(new \DateTime('NOW'));
        $this->addReference('page3', $page3);
        
        $em->persist($page1);
        $em->persist($page2);
        $em->persist($page3);

        $em->flush();

   
    }

    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}
