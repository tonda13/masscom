<?php
//load data into DB with: php app/console doctrine:fixtures:load
namespace sfepy\MasscomBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use sfepy\MasscomBundle\Entity\Script;

class LoadScriptData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $first = new Script();
        $first->setName('Sfepy');
        $first->setShortDesc('Sfepy is... (short description)');
        $first->setUrl("http://example.com");
        $first->setCmd("Console command for run python script");
        $this->addReference('scriptSfepy', $first);
        
        $second = new Script();
        $second->setName('Another one');
        $second->setShortDesc('Another one is... (short description)');
        $second->setUrl("http://example.com");
        $second->setCmd("Console command for run python script");
        $this->addReference('script2', $second);
        
        $em->persist($first);
        $em->persist($second);

        $em->flush();

   
    }

    public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }
}
