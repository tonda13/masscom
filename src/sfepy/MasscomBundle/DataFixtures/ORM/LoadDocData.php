<?php
//load data into DB with: php app/console doctrine:fixtures:load
namespace sfepy\MasscomBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use sfepy\MasscomBundle\Entity\Doc;

class LoadDocData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $page1 = new Doc();
        $page1->setTitle('Co je MASSCOM');
        $page1->setSlug('co-je-masscom');
        $page1->setTextField('Informace o této stránce...');
        $page1->setMenuOrder(1);
        $page1->setCreateAt(new \DateTime('NOW'));
        $page1->setUpdateAt(new \DateTime('NOW'));
        $this->addReference('doc1', $page1);
        
        $page2 = new Doc();
        $page2->setTitle('Jak přidat nový script');
        $page2->setSlug('jak-pridat-script');
        $page2->setTextField('Návod pro přidání nového scriptu...');
        $page2->setMenuOrder(2);
        $page2->setCreateAt(new \DateTime('NOW'));
        $page2->setUpdateAt(new \DateTime('NOW'));
        $this->addReference('doc2', $page2);
        
        $page3 = new Doc();
        $page3->setTitle('Jak spustit výpočet');
        $page3->setSlug('jak-spustit-vypocet');
        $page3->setTextField('Návod na spuštění výpočtu...');
        $page3->setMenuOrder(3);
        $page3->setCreateAt(new \DateTime('NOW'));
        $page3->setUpdateAt(new \DateTime('NOW'));
        $this->addReference('doc3', $page3);
        
        $em->persist($page1);
        $em->persist($page2);
        $em->persist($page3);

        $em->flush();

   
    }

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}
