<?php

namespace sfepy\MasscomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Script
 */
class Script
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * 
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $cmd;

    /**
     * @Assert\NotBlank()
     * 
     * @var string
     */
    private $short_desc;

    /**
     * @var string
     */
    private $full_desc;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $problem_id;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->problem_id = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Script
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Script
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set cmd
     *
     * @param string $cmd
     * @return Script
     */
    public function setCmd($cmd)
    {
        $this->cmd = $cmd;

        return $this;
    }

    /**
     * Get cmd
     *
     * @return string 
     */
    public function getCmd()
    {
        return $this->cmd;
    }

    /**
     * Set short_desc
     *
     * @param string $short_desc
     * @return Script
     */
    public function setShortDesc($short_desc)
    {
        $this->short_desc = $short_desc;

        return $this;
    }

    /**
     * Get short_desc
     *
     * @return string 
     */
    public function getShortDesc()
    {
        return $this->short_desc;
    }

    /**
     * Set full_desc
     *
     * @param string $fullDesc
     * @return Script
     */
    public function setFullDesc($fullDesc)
    {
        $this->full_desc = $fullDesc;

        return $this;
    }

    /**
     * Get full_desc
     *
     * @return string 
     */
    public function getFullDesc()
    {
        return $this->full_desc;
    }

    /**
     * Add problem_id
     *
     * @param \sfepy\MasscomBundle\Entity\Problem $problemId
     * @return Script
     */
    public function addProblemId(\sfepy\MasscomBundle\Entity\Problem $problemId)
    {
        $this->problem_id[] = $problemId;

        return $this;
    }

    /**
     * Remove problem_id
     *
     * @param \sfepy\MasscomBundle\Entity\Problem $problemId
     */
    public function removeProblemId(\sfepy\MasscomBundle\Entity\Problem $problemId)
    {
        $this->problem_id->removeElement($problemId);
    }

    /**
     * Get problem_id
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProblemId()
    {
        return $this->problem_id;
    }
    
    /**
     * Return name of entity
     * (used for selectbox for add new problem)
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}
