<?php

namespace sfepy\MasscomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Log
 */
class Log
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $create_at;

    /**
     * @var boolean
     */
    private $success;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $problem;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->problem = new \Doctrine\Common\Collections\ArrayCollection();
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
        $this->script = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set create_at
     *
     * @ORM\PrePersist
     * @param \DateTime $createAt
     * @return Log
     */
    public function setCreateAt()
    {
        if(!$this->getCreateAt()) {
            $this->create_at = new \DateTime();
        }

        return $this;
    }

    /**
     * Get create_at
     *
     * @return \DateTime 
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * Set success
     *
     * @param boolean $success
     * @return Log
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success
     *
     * @return boolean 
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * Add problem
     *
     * @param \sfepy\MasscomBundle\Entity\Problem $problem
     * @return Log
     */
    public function addProblem(\sfepy\MasscomBundle\Entity\Problem $problem)
    {
        $this->problem[] = $problem;

        return $this;
    }

    /**
     * Remove problem
     *
     * @param \sfepy\MasscomBundle\Entity\Problem $problem
     */
    public function removeProblem(\sfepy\MasscomBundle\Entity\Problem $problem)
    {
        $this->problem->removeElement($problem);
    }

    /**
     * Get problem
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProblem()
    {
        return $this->problem;
    }

    /**
     * Add user
     *
     * @param \sfepy\MasscomBundle\Entity\User $user
     * @return Log
     */
    public function addUser(\sfepy\MasscomBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \sfepy\MasscomBundle\Entity\User $user
     */
    public function removeUser(\sfepy\MasscomBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \sfepy\MasscomBundle\Entity\User $user
     * @return Log
     */
    public function setUser(\sfepy\MasscomBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Set problem
     *
     * @param \sfepy\MasscomBundle\Entity\Problem $problem
     * @return Log
     */
    public function setProblem(\sfepy\MasscomBundle\Entity\Problem $problem = null)
    {
        $this->problem = $problem;

        return $this;
    }
}
