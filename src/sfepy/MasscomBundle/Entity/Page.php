<?php

namespace sfepy\MasscomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Page
 */
class Page
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * 
     * @var string
     */
    private $title;

    /**
     * @Assert\NotBlank()
     * 
     * @var string
     */
    private $slug;

    /**
     * @Assert\NotBlank()
     * 
     * @var string
     */
    private $text_field;

    /**
     * @Assert\NotBlank()
     * 
     * @var integer
     */
    private $menu_order;

    /**
     * @var \DateTime
     */
    private $create_at;

    /**
     * @var \DateTime
     */
    private $update_at;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set text_field
     *
     * @param string $textField
     * @return Page
     */
    public function setTextField($textField)
    {
        $this->text_field = $textField;

        return $this;
    }

    /**
     * Get text_field
     *
     * @return string 
     */
    public function getTextField()
    {
        return $this->text_field;
    }

    /**
     * Set menu_order
     *
     * @param integer $menuOrder
     * @return Page
     */
    public function setMenuOrder($menuOrder)
    {
        $this->menu_order = $menuOrder;

        return $this;
    }

    /**
     * Get menu_order
     *
     * @return integer 
     */
    public function getMenuOrder()
    {
        return $this->menu_order;
    }

    /**
     * Set create_at
     *
     * @ORM\PrePersist
     * 
     * @return Doc
     */
    public function setCreateAt()
    {
        if(!$this->getCreateAt()) {
            $this->create_at = new \DateTime();
        }
        return $this;
    }


    /**
     * Get create_at
     *
     * @return \DateTime 
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * Set update_at
     *
     * @ORM\PreUpdate
     * 
     * @return Doc
     */
    public function setUpdateAt()
    {
        $this->update_at = new \DateTime();

        return $this;
    }

    /**
     * Get update_at
     *
     * @return \DateTime 
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }
}
