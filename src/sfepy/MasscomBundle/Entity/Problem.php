<?php

namespace sfepy\MasscomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Problem
 */
class Problem
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * 
     * @var string
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * 
     * @var string
     */
    private $xml_schema;

    /**
     * @var string
     */
    private $short_desc;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $user_save;

    /**
     * @Assert\NotBlank()
     * 
     * @var \sfepy\MasscomBundle\Entity\Script
     */
    private $script;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user_save = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Problem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set xml_schema
     *
     * @param string $xmlSchema
     * @return Problem
     */
    public function setXmlSchema($xmlSchema)
    {
        $this->xml_schema = $xmlSchema;

        return $this;
    }

    /**
     * Get xml_schema
     *
     * @return string 
     */
    public function getXmlSchema()
    {
        return $this->xml_schema;
    }
    
        /**
     * Set short_desc
     *
     * @param string $short_desc
     * @return Script
     */
    public function setShortDesc($short_desc)
    {
        $this->short_desc = $short_desc;

        return $this;
    }

    /**
     * Get short_desc
     *
     * @return string 
     */
    public function getShortDesc()
    {
        return $this->short_desc;
    }

    /**
     * Set script
     *
     * @param \sfepy\MasscomBundle\Entity\Script $script
     * @return Problem
     */
    public function setScript(\sfepy\MasscomBundle\Entity\Script $script = null)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script
     *
     * @return \sfepy\MasscomBundle\Entity\Script 
     */
    public function getScript()
    {
        return $this->script;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $log;


    /**
     * Add log
     *
     * @param \sfepy\MasscomBundle\Entity\Log $log
     * @return Problem
     */
    public function addLog(\sfepy\MasscomBundle\Entity\Log $log)
    {
        $this->log[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \sfepy\MasscomBundle\Entity\Log $log
     */
    public function removeLog(\sfepy\MasscomBundle\Entity\Log $log)
    {
        $this->log->removeElement($log);
    }

    /**
     * Get log
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Add user_save
     *
     * @param \sfepy\MasscomBundle\Entity\Usersave $userSave
     * @return Problem
     */
    public function addUserSave(\sfepy\MasscomBundle\Entity\Usersave $userSave)
    {
        $this->user_save[] = $userSave;

        return $this;
    }

    /**
     * Remove user_save
     *
     * @param \sfepy\MasscomBundle\Entity\Usersave $userSave
     */
    public function removeUserSave(\sfepy\MasscomBundle\Entity\Usersave $userSave)
    {
        $this->user_save->removeElement($userSave);
    }

    /**
     * Get user_save
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserSave()
    {
        return $this->user_save;
    }
    
    /**
     * Return name of entity
     * (used for selectbox for add new problem)
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}