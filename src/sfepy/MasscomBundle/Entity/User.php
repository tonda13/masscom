<?php

namespace sfepy\MasscomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
//    private $flag;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $user_save;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $log;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->user_save = new \Doctrine\Common\Collections\ArrayCollection();
        $this->log = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set flag
     *
     * @param string $flag
     * @return User
     */
    /*public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }*/

    /**
     * Get flag
     *
     * @return string 
     */
    /*public function getFlag()
    {
        return $this->flag;
    }*/

    /**
     * Add user_save
     *
     * @param \sfepy\MasscomBundle\Entity\Usersave $userSave
     * @return User
     */
    public function addUserSave(\sfepy\MasscomBundle\Entity\Usersave $userSave)
    {
        $this->user_save[] = $userSave;

        return $this;
    }

    /**
     * Remove user_save
     *
     * @param \sfepy\MasscomBundle\Entity\Usersave $userSave
     */
    public function removeUserSave(\sfepy\MasscomBundle\Entity\Usersave $userSave)
    {
        $this->user_save->removeElement($userSave);
    }

    /**
     * Get user_save
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserSave()
    {
        return $this->user_save;
    }

    /**
     * Add log
     *
     * @param \sfepy\MasscomBundle\Entity\Log $log
     * @return User
     */
    public function addLog(\sfepy\MasscomBundle\Entity\Log $log)
    {
        $this->log[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \sfepy\MasscomBundle\Entity\Log $log
     */
    public function removeLog(\sfepy\MasscomBundle\Entity\Log $log)
    {
        $this->log->removeElement($log);
    }

    /**
     * Get log
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLog()
    {
        return $this->log;
    }
    
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }
    
    public function getCredentialsExpireAt()
    {
        return $this->credentialsExpireAt;
    }
    
    public function getPasswd(){
        return $this->password;
    }
        
}
