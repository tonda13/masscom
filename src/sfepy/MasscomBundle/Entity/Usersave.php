<?php

namespace sfepy\MasscomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usersave
 */
class Usersave
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $datafile;

    /**
     * @var string
     */
    private $name;
    
    /**
     * @var \sfepy\MasscomBundle\Entity\User
     */
    private $user;

    /**
     * @var \sfepy\MasscomBundle\Entity\Problem
     */
    private $problem;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datafile
     *
     * @param string $datafile
     * @return Usersave
     */
    public function setDatafile($datafile)
    {
        $this->datafile = $datafile;

        return $this;
    }

    /**
     * Get datafile
     *
     * @return string 
     */
    public function getDatafile()
    {
        return $this->datafile;
    }

    /**
     * Set user
     *
     * @param \sfepy\MasscomBundle\Entity\User $user
     * @return Usersave
     */
    public function setUser(\sfepy\MasscomBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \sfepy\MasscomBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return Usersave
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set problem
     *
     * @param \sfepy\MasscomBundle\Entity\Problem $problem
     * @return Usersave
     */
    public function setProblem(\sfepy\MasscomBundle\Entity\Problem $problem = null)
    {
        $this->problem = $problem;

        return $this;
    }

    /**
     * Get problem
     *
     * @return \sfepy\MasscomBundle\Entity\Problem 
     */
    public function getProblem()
    {
        return $this->problem;
    }

}
